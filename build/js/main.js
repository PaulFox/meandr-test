(function() {
    "use strict";
    var feedbackForm = document.forms.feedback_form,
        telInput = feedbackForm.elements.phone_num,
        regExpEmail = /^\w+([\.\-]?\w+)*@\w+([\.\-]?\w+)*(\.\w{2,3})+$/;

    feedbackForm.noValidate = true;

    feedbackForm.onsubmit = function(event) {
        validate(event, this);
        console.log('red');
    };

    telInput.onkeyup = function() {
        formatTel(this);
    };

    telInput.onblur = function() {
        if (this.value.length === 4) {
            this.value = "";
        }
    };

    [].forEach.call(feedbackForm.elements, function(item) {
        item.addEventListener('focus', function() {
            this.parentNode.classList.remove("form-error");
        });
    });

    function validate(event, form) {
        var elems = form.elements,
            isEmpty,
            notValid;

        resetError(elems, form);

        if (!elems.full_name.value) {
            elems.full_name.parentNode.classList.add("form-error");
            isEmpty = true;
        }

        if (!elems.phone_num.value) {
            elems.phone_num.parentNode.classList.add("form-error");
            isEmpty = true;
        }

        if (elems.phone_num.value.length !== 18) {
            elems.phone_num.parentNode.classList.add("form-error");
            notValid = true;
        }

        if (!elems.email.value) {
            elems.email.parentNode.classList.add("form-error");
            isEmpty = true;
        }
        if (!regExpEmail.test(elems.email.value)) {
            elems.email.parentNode.classList.add("form-error");
            notValid = true;
        }

        if (!elems.feedback.value) {
            elems.feedback.parentNode.classList.add("form-error");
            isEmpty = true;
        }

        if (isEmpty || notValid) {
            event.preventDefault();
        }
    }

    function resetError(elems, form) {

        [].forEach.call(elems, function(item) {
            item.parentNode.classList.remove("form-error");
        });
    }

    function formatTel(telElem) {

        var num = telElem.value.replace("+7", "").replace(/\D/g, "").split(/(?=.)/),
            i = num.length;

        if (0 <= i) num.unshift("+7");

        if (1 <= i) num.splice(1, 0, " ");

        if (1 <= i) num.splice(2, 0, "(");

        if (3 <= i) num.splice(6, 0, ") ");

        if (6 <= i) num.splice(10, 0, "-");

        if (8 <= i) num.splice(13, 0, "-");

        if (11 <= i) num.splice(16, num.length - 16);

        telElem.value = num.join("");
    };
})();